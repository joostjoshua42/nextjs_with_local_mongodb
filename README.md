# Boilerplate für ein NextJS-Projekt mit einer lokalen MongoDB Anbindung #
# Setup Steps
## __MongoDB installieren__
Die MongoDB Software kann beispielsweise [hier](https://www.mongodb.com/try/download/community?tck=docs_server) heruntergeladen werden (für diesen Vorgang sind lokale Administratorrechte notwendig).
Den Installationswizard in seiner Voreinstellung einfach durchklicken.

### __Speicherort der MongoDB bestimmen__
Wenn der MongoDB Server ausgeführt wird, wird ihm als Parameter der Speicherort für die DB übergeben. <br/>
Bsp. Anlegen des Speicherortes für die DB:<br/>
- Beliebige cmd öffnen <br/>
- `cd c:/`
- `md data/db`

Mit diesem Befehl werden auf Laufwerk C die verschachtelten Ordner data/db angelegt.

### __NextJS_with_local_MongoDB Boilerplate herunterladen__
GitLab-Projekt via `git clone <repository:Clone with Https>` herunterladen. <br/>
Hint: Fals noch nicht geschehen vorab die git proxy Einstellungen durchführen: <br/>
`git config --global http.proxy http://NNummer:NPasswort@proxy1-s.gavi-intra.de:8080`

## **MongoDB starten**
"C:\Program Files\MongoDB\Server\4.4\bin\mongod.exe" --dbpath "data\db" <br/>
Der Parameter --dbpath geht immer von der Festplatte aus, auf der die MongoDB selbst installiert ist. Hier entsprechend von C:\

## **NextJS Projekt starten**
### **Umgebungsvariable setzen**
Bevor das Projekt gestartet werden kann muss zunächst noch die Umgebungsvariable für die MongoDB-Verbindung gesetzt werden. <br/>
Dazu im Wurzelverzeichnis eine _.env.local_ Datei erstellen und in dieser die Variable für de MongoDB Verbindung eintragen. Bspw.: <br/> ``MONGO_URI = "mongodb://127.0.0.1:27017/"`` <br/>
Auf diese Variable kann im Frontend nicht zugegriffen werden, sie ist also geschützt.

### **Umgebungsvariable entsprechend der MongoDB Verbindung anpassen**
Im Wurzelverzeichnis des NextJs Projektes findet sich die *next.config.js* Datei, die die Umgebungsvariable *MONGO_URI* enthält. Die Variable entsprechend der MongoDB Connection anpassen (bspw. könnte der Wert so aussehen: "mongodb://127.0.0.1:27017/").
<br/>
Anschließend das Projekt starten:
- `yarn dev`
- `npx run dev`

Die URL *http://localhost:3000/api/test* aufrufen. <br/>
War die DB-Verbindung erfolgreich wird in der VS-Code Konsole "Igor: We are connected Sir!" ausgegeben.

## **Props**
Das Projekt ist stark inspiriert durch die Videoreihe 'Build an app with Next JS and MongoDB' des Youtubers Jason Rivera.





