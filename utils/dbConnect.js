import mongoose from 'mongoose';

const connection = {};

async function dbConnect(){
    if (connection.isConnected){
        return;
    }
    
    const db = await mongoose.connect(process.env.MONGO_URI, {
        // set up properties to get rid of some warning 
        // that we may see in the console from mongoose
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });

    connection.isConnected = db.connections[0].readyState;
    // console.log(connection.isConnected);
    if (connection.isConnected){
        console.log("Igor: We are connected Sir!")
    }
}

export default dbConnect;