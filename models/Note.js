const mongoose = require('mongoose');

const NoteSchema = new mongoose.Schema({
    title: {
        type: String,
        required: [true, 'Please add a title'],
        unique: true,
        trim: true,
        maxlength: [40, 'Title could not be longer than 40 characters']      
    },
    description: {
         type: String,
         required: true,
         maxlength: [200, 'Description could not be longer than 200 characters']
    }
})

// if mongoose.models.Note exists already go ahead and export that
// if not create the model which is going to be 'Note' and pass it the Scheme definition
module.exports = mongoose.models.Note || mongoose.model('Note',NoteSchema);